from calculations import forward, predict
from sklearn.datasets import load_iris
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns; sns.set_style("whitegrid")
from matplotlib.colors import LinearSegmentedColormap
import pandas as pd

# w_0 = [[-0.5, 0.2],
#        [0.4, -0.1],
#        [0.1, 0]]
# w_1 = [[0.73, 0.97, 0.85],
#        [-0.23, -0.72, 0.17]]

# b_0 = [-0.2, 0, 0.1]
# b_1 = [0.73, -0.37]

x_min = 4
x_max = 8
x_values = 5

y_min = 1
y_max = 5
y_values = 5

w_0 = [[-0.61675045,  1.00336458],
 [-0.42481386, -0.13503415],
 [-0.40619855, -0.12879465]]
w_1 = [[ 1.0620973,   0.70840219,  0.67423439],
 [-0.6683703,  -0.53788711,  0.2573834 ]]

b_0 = [-0.09888264, -0.09007328,  0.0292278 ]
b_1 = [-0.42298454,  0.19780145]

max_value = 1
min_value = max_value * -1

iris = load_iris()

data = iris['data'][:100,:2]
targets = iris['target'][:100]

def get_value(x, y):

    i = [x, y]

    p, q = forward(w_0=w_0, w_1=w_1, b_0=b_0, b_1=b_1, i=i)

    return min_value if p > q else max_value

# print(get_value(5.4, 3.7))







def plot_heatmap():
    arr = np.zeros((x_values, y_values))

    for i, x in enumerate(np.linspace(x_min, x_max, x_values)):
        for j, y in enumerate(np.linspace(y_max, y_min, y_values)):
            arr[j, i] = get_value(x, y)

    myColors = ((0.6313725490196078, 0.788235294117647, 0.9568627450980393), (1.0, 0.7058823529411765, 0.5098039215686274))
    cmap = LinearSegmentedColormap.from_list('Custom', myColors, len(myColors))

    ax = sns.heatmap(arr, cmap=cmap, cbar=False, center=0, vmax=max_value)

    return ax

def plot_scatter(ax):

    for i, t in zip(data, targets):
        p = predict(w_0=w_0, w_1=w_1, b_0=b_0, b_1=b_1, i=i)
        correct = p == t

        x, y = i

        print()

    scatter_colours = dict({0:'red', 1:'green'})
    sns.scatterplot(x=[25, 30], y=[40, 40], hue=[0, 1], ax=ax, marker="+", palette=scatter_colours, s=50, legend=None)



# ax = plot_heatmap()
# plot_scatter(ax)

print()
print()

data = []
for x in np.linspace(x_min, x_max, x_values):
    for y in np.linspace(y_min, y_max, y_values):
        v = get_value(x, y)

        data.append([x, y, v])

data = np.array(data)

df = pd.DataFrame(data=data, columns=["X", "Y", "Values"])
# print(df)

df2 = df.pivot("Y", "X", "Values")
# print(df2)
myColors = ((0.6313725490196078, 0.788235294117647, 0.9568627450980393), (1.0, 0.7058823529411765, 0.5098039215686274))
cmap = LinearSegmentedColormap.from_list('Custom', myColors, len(myColors))
ax = sns.heatmap(df2, cmap=cmap, cbar=False, center=0, vmax=max_value)
ax.invert_yaxis()
# xticklabels=np.linspace(x_min, x_max, x_values), yticklabels=np.linspace(y_max, y_min, y_values), 


x_lim_min, x_lim_max = ax.get_xlim()
y_lim_min, y_lim_max = ax.get_ylim()
x_lim_min = x_lim_min + 0.5
x_lim_max = x_lim_max - 0.5
x_lim_range = x_lim_max - x_lim_min
y_lim_min = y_lim_min + 0.5
y_lim_max = y_lim_max - 0.5
y_lim_range = y_lim_max - y_lim_min

def transform_x(x):
    percent = (x - x_min) / (x_max - x_min)
    return (percent * (x_lim_range)) + x_lim_min

def transform_y(y):
    percent = (y - y_min) / (y_max - y_min)
    return (percent * (y_lim_range)) + y_lim_min

# print(x_lim_min, x_lim_max, x_lim_range)
# print(y_lim_min, y_lim_max, y_lim_range)

scatter_colours = dict({0:'red', 1:'green'})
sns.scatterplot(x=[], y=[4], hue=[1], ax=ax, marker="+", palette=scatter_colours, s=50, legend=None)

plt.show()
