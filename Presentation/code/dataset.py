from sklearn.datasets import load_iris
from sklearn.metrics import mean_squared_error
import numpy as np
from calculations import forward

iris = load_iris()

data = iris['data'][:100,:2]
targets = iris['target'][:100]
targets_arr = np.array([[1,0] if t == 0 else [0,1] for t in iris['target'][:100]])

# print(len(data))
# print(data)
# print(targets)
# print(type(data))
# print(type(targets))

w_0 = np.array([[-0.5, 0.2],
       [0.4, -0.1],
       [0.1, 0]])
w_1 = np.array([[0.73, 0.97, 0.85],
       [-0.23, -0.72, 0.17]])

b_0 = np.array([-0.2, 0, 0.1])
b_1 = np.array([0.73, -0.37])

def mse(input_values, targets, w_0=w_0, w_1=w_1, b_0=b_0, b_1=b_1):
    predictions = []

    for input_value in input_values:
        out_o = forward(w_0=w_0, w_1=w_1, b_0=b_0, b_1=b_1, i=input_value)
        predictions.append(out_o)

    predictions = np.array(predictions)

    return mean_squared_error(targets, predictions)

def correct(input_values, targets, w_0=w_0, w_1=w_1, b_0=b_0, b_1=b_1):
    correct = 0

    for input_value, target in zip(data, targets):
        out_o = forward(w_0=w_0, w_1=w_1, b_0=b_0, b_1=b_1, i=input_value)
        index = np.argmax(out_o)
        target_index = np.argmax(target)

        # print(index, target_index)

        if index == target_index:
            correct += 1

    return correct

# predictions = []

# for input_values, target, target_arr in zip(data, targets, targets_arr):
#     out_o = forward(i=input_values)
#     predictions.append(out_o)
#     index = np.argmax(out_o)

#     diff = abs(out_o[0] - out_o[1])
#     # value = out_o[index]
#     value = out_o[1]

#     tmp_mse = mean_squared_error(target_arr, out_o)

#     # print(input_values, target, index == target, value)
#     # print(out_o, target_arr, tmp_mse, index == target)

#     if index == target:
#         correct += 1

# # print(correct)

# predictions = np.array(predictions)

# print(targets_arr)
# print(predictions)

# mse = mean_squared_error(targets_arr, predictions)


def main():
    print(mse(data, targets_arr))

if __name__ == "__main__":
    main()
