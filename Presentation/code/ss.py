from sklearn.metrics import mean_squared_error

y_true = [[1, 0]]
y_pred = [[0.900, 0.289]]
res = mean_squared_error(y_true, y_pred)

print(res)


x = (1 - 0.900 + 0.289 - 0)**2
y = ((1 - 0.900)**2 + (0.289 - 0)**2 ) / 2

print(x)
print(y)