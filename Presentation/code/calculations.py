import numpy as np

def sigmoid(x):
  return 1.0 / (1.0 + np.exp(-x))

def error(target, output):
    return 0.5 * (target - output)**2

# 2-3-2 NN

i = [5.4, 3.7]
# i = [5.8, 2.7]
# i = [6.7, 3.0]

target_o = [1, 0]

w_0 = [[-0.5, 0.2],
       [0.4, -0.1],
       [0.1, 0]]
w_1 = [[0.73, 0.97, 0.85],
       [-0.23, -0.72, 0.17]]

b_0 = [-0.2, 0, 0.1]
b_1 = [0.73, -0.37]

def forward_detailed(w_0=w_0, w_1=w_1, b_0=b_0, b_1=b_1, i=i, target_o=target_o):
    net_h = [w_0[0][0] * i[0] + w_0[0][1] * i[1] + b_0[0],
            w_0[1][0] * i[0] + w_0[1][1] * i[1] + b_0[1],
            w_0[2][0] * i[0] + w_0[2][1] * i[1] + b_0[2]]

    out_h = [sigmoid(net_h[0]),
            sigmoid(net_h[1]),
            sigmoid(net_h[2])]

    net_o = [w_1[0][0] * out_h[0] + w_1[0][1] * out_h[1] + w_1[0][2] * out_h[2] + b_1[0],
            w_1[1][0] * out_h[0] + w_1[1][1] * out_h[1] + w_1[1][2] * out_h[2] + b_1[1]]

    out_o = [sigmoid(net_o[0]),
            sigmoid(net_o[1])]

    error_o = [error(target_o[0], out_o[0]),
            error(target_o[1], out_o[1])]
    error_total = sum(error_o)

    return net_h, out_h, net_o, out_o, error_o, error_total

def forward(w_0=w_0, w_1=w_1, b_0=b_0, b_1=b_1, i=i, target_o=target_o):
    _, _, _, out_o, _, _ = forward_detailed(w_0, w_1, b_0, b_1, i, target_o)

    return out_o

def predict(w_0=w_0, w_1=w_1, b_0=b_0, b_1=b_1, i=i):
    out_o = forward(w_0=w_0, w_1=w_1, b_0=b_0, b_1=b_1, i=i)

    return np.argmax(out_o)

def backprop_detailed(w_0=w_0, w_1=w_1, b_0=b_0, b_1=b_1, i=i, target_o=target_o):

    net_h, out_h, net_o, out_o, error_o, error_total = forward_detailed(w_0, w_1, b_0, b_1, i, target_o)

    # Output layer weights
    w_1_delta_total = [[0,0,0], [0,0,0]]
    w_1_delta_c0 = [[0,0,0], [0,0,0]]
    w_1_delta_c1 = [[0,0,0], [0,0,0]]
    w_1_delta_c2 = [[0,0,0], [0,0,0]]

    # Output layer bias
    b_1_delta_total = [0,0]
    b_1_delta_c0 = [0,0]
    b_1_delta_c1 = [0,0]

    for neuron_layer_1 in range(2):
        b_1_delta_c0[neuron_layer_1] = out_o[neuron_layer_1] - target_o[neuron_layer_1]
        b_1_delta_c1[neuron_layer_1] = out_o[neuron_layer_1] * (1 - out_o[neuron_layer_1])

        b_1_delta_total[neuron_layer_1] = b_1_delta_c0[neuron_layer_1] * b_1_delta_c1[neuron_layer_1]

        for neuron_layer_0 in range(3):
            w_1_delta_c0[neuron_layer_1][neuron_layer_0] = out_o[neuron_layer_1] - target_o[neuron_layer_1]
            w_1_delta_c1[neuron_layer_1][neuron_layer_0] = out_o[neuron_layer_1] * (1 - out_o[neuron_layer_1])
            w_1_delta_c2[neuron_layer_1][neuron_layer_0] = out_h[neuron_layer_0]

            w_1_delta_total[neuron_layer_1][neuron_layer_0] =  w_1_delta_c0[neuron_layer_1][neuron_layer_0] * w_1_delta_c1[neuron_layer_1][neuron_layer_0] * w_1_delta_c2[neuron_layer_1][neuron_layer_0]

    w_1_delta = [w_1_delta_total, w_1_delta_c0, w_1_delta_c1, w_1_delta_c2]
    b_1_delta = [b_1_delta_total, b_1_delta_c0, b_1_delta_c1]

    # Hidden layer weights
    w_0_delta_total = [[0,0], [0,0], [0,0]]
    w_0_delta_c0_0 = [[0,0], [0,0], [0,0]]
    w_0_delta_c0_1 = [[0,0], [0,0], [0,0]]
    w_0_delta_c0_2 = [[0,0], [0,0], [0,0]]

    w_0_delta_c1_0 = [[0,0], [0,0], [0,0]]
    w_0_delta_c1_1 = [[0,0], [0,0], [0,0]]
    w_0_delta_c1_2 = [[0,0], [0,0], [0,0]]

    w_0_delta_c2 = [[0,0], [0,0], [0,0]]
    w_0_delta_c3 = [[0,0], [0,0], [0,0]]

    # Hidden layer bias
    b_0_delta_total = [0, 0, 0]
    b_0_delta_c0_0 = [0, 0, 0]
    b_0_delta_c0_1 = [0, 0, 0]
    b_0_delta_c0_2 = [0, 0, 0]

    b_0_delta_c1_0 = [0, 0, 0]
    b_0_delta_c1_1 = [0, 0, 0]
    b_0_delta_c1_2 = [0, 0, 0]

    b_0_delta_c2 = [0, 0, 0]

    for neuron_layer_1 in range(3):
        b_0_delta_c0_0[neuron_layer_1] = out_o[0] - target_o[0]
        b_0_delta_c0_1[neuron_layer_1] = out_o[0] * (1 - out_o[0])
        b_0_delta_c0_2[neuron_layer_1] = w_1[0][neuron_layer_1]

        b_0_delta_c1_0[neuron_layer_1] = out_o[1] - target_o[1]
        b_0_delta_c1_1[neuron_layer_1] = out_o[1] * (1 - out_o[1])
        b_0_delta_c1_2[neuron_layer_1] = w_1[1][neuron_layer_1]

        b_0_delta_c2[neuron_layer_1] = out_h[neuron_layer_1] * (1 - out_h[neuron_layer_1])

        b_0_delta_total[neuron_layer_1] = (b_0_delta_c0_0[neuron_layer_1] * b_0_delta_c0_1[neuron_layer_1] * b_0_delta_c0_2[neuron_layer_1] + b_0_delta_c1_0[neuron_layer_1] * b_0_delta_c1_1[neuron_layer_1] * b_0_delta_c1_2[neuron_layer_1]) * b_0_delta_c2[neuron_layer_1]
        
        for neuron_layer_0 in range(2):
            w_0_delta_c0_0[neuron_layer_1][neuron_layer_0] = out_o[0] - target_o[0]
            w_0_delta_c0_1[neuron_layer_1][neuron_layer_0] = out_o[0] * (1 - out_o[0])
            w_0_delta_c0_2[neuron_layer_1][neuron_layer_0] = w_1[0][neuron_layer_1]

            w_0_delta_c1_0[neuron_layer_1][neuron_layer_0] = out_o[1] - target_o[1]
            w_0_delta_c1_1[neuron_layer_1][neuron_layer_0] = out_o[1] * (1 - out_o[1])
            w_0_delta_c1_2[neuron_layer_1][neuron_layer_0] = w_1[1][neuron_layer_1]

            w_0_delta_c2[neuron_layer_1][neuron_layer_0] = out_h[neuron_layer_1] * (1 - out_h[neuron_layer_1])
            w_0_delta_c3[neuron_layer_1][neuron_layer_0] = i[neuron_layer_0]

            w_0_delta_total[neuron_layer_1][neuron_layer_0] = (w_0_delta_c0_0[neuron_layer_1][neuron_layer_0] * w_0_delta_c0_1[neuron_layer_1][neuron_layer_0] * w_0_delta_c0_2[neuron_layer_1][neuron_layer_0] + w_0_delta_c1_0[neuron_layer_1][neuron_layer_0] * w_0_delta_c1_1[neuron_layer_1][neuron_layer_0] * w_0_delta_c1_2[neuron_layer_1][neuron_layer_0]) * w_0_delta_c2[neuron_layer_1][neuron_layer_0] * w_0_delta_c3[neuron_layer_1][neuron_layer_0] 

    w_0_delta = [w_0_delta_total, w_0_delta_c0_0, w_0_delta_c0_1, w_0_delta_c0_2, w_0_delta_c1_0, w_0_delta_c1_1, w_0_delta_c1_2, w_0_delta_c2, w_0_delta_c3]
    b_0_delta = [b_0_delta_total, b_0_delta_c0_0, b_0_delta_c0_1, b_0_delta_c0_2, b_0_delta_c1_0, b_0_delta_c1_1, b_0_delta_c1_2, b_0_delta_c2]

    return net_h, out_h, net_o, out_o, error_o, error_total, w_1_delta, b_1_delta, w_0_delta, b_0_delta

def backprop(w_0=w_0, w_1=w_1, b_0=b_0, b_1=b_1, i=i, target_o=target_o):
    net_h, out_h, net_o, out_o, error_o, error_total, w_1_delta, b_1_delta, w_0_delta, b_0_delta = backprop_detailed(w_0, w_1, b_0, b_1, i, target_o)

    w_1_delta_total, _, _, _ = w_1_delta
    b_1_delta_total, _, _ = b_1_delta

    w_0_delta_total, _, _, _, _, _, _, _, _ = w_0_delta
    b_0_delta_total, _, _, _, _, _, _, _ = b_0_delta

    return w_0_delta_total, w_1_delta_total, b_0_delta_total, b_1_delta_total

def epoch(input_values, targets, w_0, w_1, b_0, b_1):
    all_w_0_delta_total = []
    all_w_1_delta_total = []
    all_b_0_delta_total = []
    all_b_1_delta_total = []

    for i, target_o in zip(input_values, targets):
        w_0_delta_total, w_1_delta_total, b_0_delta_total, b_1_delta_total = backprop(w_0, w_1, b_0, b_1, i, target_o)

        all_w_0_delta_total.append(w_0_delta_total)
        all_w_1_delta_total.append(w_1_delta_total)
        all_b_0_delta_total.append(b_0_delta_total)
        all_b_1_delta_total.append(b_1_delta_total)

    w_0_deltas = np.array(all_w_0_delta_total).sum(axis=0) / 100
    w_1_deltas = np.array(all_w_1_delta_total).sum(axis=0) / 100
    b_0_deltas = np.array(all_b_0_delta_total).sum(axis=0) / 100
    b_1_deltas = np.array(all_b_1_delta_total).sum(axis=0) / 100

    # print(w_0_deltas)

    new_w_0 = w_0 - w_0_deltas
    new_w_1 = w_1 - w_1_deltas
    new_b_0 = b_0 - b_0_deltas
    new_b_1 = b_1 - b_1_deltas

    return new_w_0, new_w_1, new_b_0, new_b_1

def main():

    net_h, out_h, net_o, out_o, error_o, error_total, w_1_delta, b_1_delta, w_0_delta, b_0_delta = backprop_detailed()

    w_1_delta_total, w_1_delta_c0, w_1_delta_c1, w_1_delta_c2 = w_1_delta
    b_1_delta_total, b_1_delta_c0, b_1_delta_c1 = b_1_delta

    w_0_delta_total, w_0_delta_c0_0, w_0_delta_c0_1, w_0_delta_c0_2, w_0_delta_c1_0, w_0_delta_c1_1, w_0_delta_c1_2, w_0_delta_c2, w_0_delta_c3 = w_0_delta
    b_0_delta_total, b_0_delta_c0_0, b_0_delta_c0_1, b_0_delta_c0_2, b_0_delta_c1_0, b_0_delta_c1_1, b_0_delta_c1_2, b_0_delta_c2 = b_0_delta

    print("Hidden Layer")

    print("net_h0", net_h[0])
    print("net_h1", net_h[1])
    print("net_h2", net_h[2])

    print("out_h0", out_h[0])
    print("out_h1", out_h[1])
    print("out_h2", out_h[2])

    print("\nOutput Layer")

    print("net_o0", net_o[0])
    print("net_o1", net_o[1])

    print("out_o0", out_o[0])
    print("out_o1", out_o[1])

    print("\nErrors")

    print("error_o0", error_o[0])
    print("error_o1", error_o[1])
    print("error_total", error_total)

    print("\nDerivatives")
    # print("d_error_total_out_o0", d_error_total_out_o0)
    # print("d_out_o0_net_o0", d_out_o0_net_o0)
    # print("d_net_o0_w_1_0", d_net_o0_w_1_0)
    # print("d_error_total_w_1_0", d_error_total_w_1_0)
    # print("d_error_total_w_1_0", x)

    print("\nLayer 1 Weight Deltas")
    print(w_1_delta_c0)
    print(w_1_delta_c1)
    print(w_1_delta_c2)
    print(w_1_delta_total)

    print("\nLayer 1 Weights & New Weights")
    print(w_1)
    # print(new_w_1)

    print("\nLayer 1 Bias Deltas")
    print(b_1_delta_c0)
    print(b_1_delta_c1)
    print(b_1_delta_total)

    print("\nLayer 1 Bias & New Bias")
    print(b_1)
    # print(new_b_1)

    print("\nLayer 0 Weight Deltas")
    print(w_0_delta_c0_0)
    print(w_0_delta_c0_1)
    print(w_0_delta_c0_2)
    print(w_0_delta_c1_0)
    print(w_0_delta_c1_1)
    print(w_0_delta_c1_2)
    print(w_0_delta_c2)
    print(w_0_delta_c3)
    print(w_0_delta_total)

    print("\nLayer 0 Bias Deltas")
    print(b_0_delta_c0_0)
    print(b_0_delta_c0_1)
    print(b_0_delta_c0_2)
    print(b_0_delta_c1_0)
    print(b_0_delta_c1_1)
    print(b_0_delta_c1_2)
    print(b_0_delta_c2)
    print(b_0_delta_total)



#     print("new_w_1_0", new_w_1[0][0])


#     print("out_o0 - target", out_o[0] - target_o[0])
#     print("out_o0 (1 - out_o0)", out_o[0] * (1 - out_o[0]))
#     print("out_h[0]", out_h[0])
#     print("out_h[0]", out_h[1])
#     print("res", (out_o[0] - target_o[0]) * (out_o[0] * (1 - out_o[0])) * out_h[0])
#     print("res", (out_o[0] - target_o[0]) * (out_o[0] * (1 - out_o[0])) * out_h[1])
#     print("res", (out_o[0] - target_o[0]) * (out_o[0] * (1 - out_o[0])) * out_h[2])

#     print("res 3", (out_o[1] - target_o[1]) * (out_o[1] * (1 - out_o[1])) * out_h[0])
#     print("res 4", (out_o[1] - target_o[1]) * (out_o[1] * (1 - out_o[1])) * out_h[1])
#     print("res 5", (out_o[1] - target_o[1]) * (out_o[1] * (1 - out_o[1])) * out_h[2])


    # x =  1 - out_h[0]
    # y = out_h[0] * x

    # print(x)
    # print(y)







if __name__ == "__main__":
    main()