import matplotlib.pyplot as plt
import seaborn as sns; sns.set_style("whitegrid")
from epochs import epochs

x, mse, accuracy, _, _, _, _ = list(zip(*epochs()))

# print(x)
# print(mse)
# print(accuracy)

ax = sns.lineplot(x=x, y=accuracy)

ax.set_title("Accuracy")
ax.set_xlabel("Epoch")
ax.set_ylabel("%")
ax.set_ylim(48, 102)

plt.show()
