import matplotlib.pyplot as plt
import numpy as np
np.random.seed(0)

def f(a, b):
  if a + b > 1:
    return 1
  return 0

def g(a, b):
  if a > 0.9 or b**a > 0.8:
    return 1
  return 0

in_data = np.random.random_sample((2000, 2))
result = np.array(list(map(lambda x : f(x[0], x[1]), in_data)))



def get_data():
    return in_data, result

# print(result)
# print(sum(result))

plt.scatter(in_data[:, 0], in_data[:, 1], c=result)
plt.show()

# for a, b in r:
#   print(f(a, b))
