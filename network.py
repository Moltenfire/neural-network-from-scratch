import numpy as np
np.random.seed(0)

def get_random(min_val=0, max_val=1, *args):
  return np.random.random_sample(args) * (max_val - min_val) + min_val

def sigmoid(x):
  return 1.0 / (1.0 + np.exp(-x))

def sigmoid_prime(x):
    return sigmoid(x) * (1 - sigmoid(x))

class Neuron():
  def __init__(self, num_in):
    self.num_in = num_in
    self.weights = get_random(-1, 1, self.num_in)
    self.bias = get_random(-1, 1)
    self.epoch = 0

  def activate(self, in_data):
    assert(len(in_data) == self.num_in)
    res = 0
    for weight, value in zip(self.weights, in_data):
      res += weight * value
    res += self.bias
    return sigmoid(res)

  def __repr__(self):
    return "W: {} B: {}".format(self.weights, self.bias)

class Layer():
  def __init__(self, num_in, num_out):
    self.num_in = num_in
    self.num_out = num_out
    self.neurons = [Neuron(self.num_in) for _ in range(self.num_out)]

  def forward(self, in_data):
    assert(len(in_data) == self.num_in)

    result = []
    for neuron in self.neurons:
      result.append(neuron.activate(in_data))
    return result

  def __repr__(self):
    s = "{} => {}".format(self.num_in, self.num_out)
    for n in self.neurons:
      s += "\n  {}".format(n)
    return s

layer1 = Layer(2, 3)
layer2 = Layer(3, 2)

# print(layer1)
# print(layer2)

x = [0.2, 0.4]
y = [1, 0]

a2 = layer1.forward(x)
a3 = layer2.forward(a2)

print(a2)
print(a3)

# print(n)

# n0 = Neuron(3)
# n1 = Neuron(3)


# print(n0.activate(i))
# print(n1.activate(i))