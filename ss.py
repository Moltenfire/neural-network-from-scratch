# from nn import Network
from nn_mat import Network
from tt import get_data
import numpy as np
import matplotlib.pyplot as plt
from math import ceil

def indices_to_one_hot(data, nb_classes):
    """Convert an iterable of indices to one-hot encoded labels."""
    targets = np.array(data).reshape(-1)
    return np.eye(nb_classes)[targets]

def accuracy(nn, test_xs, test_ys):
    result = np.array([np.argmax(nn.forward(x)) for x, y in zip(test_xs, test_ys)])
    c = np.sum(test_ys == result)
    t = len(test_xs)
    print(F"Epoch {nn.epoch} {c}/{t} {c/t*100:.4}%")
    return result*2 + test_ys

def plot(test_xs, results):

    rows = ceil(len(results)/5)
    fig, ax = plt.subplots(nrows=rows, ncols=5, sharex=True, sharey=True)

    colours = {
        0 : '#ff0000',
        1 : '#770000',
        2 : '#007700',
        3 : '#00ff00'
    }

    for i, result in enumerate(results):
        a, b = int(i/5), i % 5
        axis = ax[a,b] if rows > 1 else ax[b]
        l = axis.scatter(test_xs[:, 0], test_xs[:, 1], c=[colours[r] for r in result])

    plt.show()

nn = Network((2, 8, 2))

xs, ys = get_data()

training_xs = xs[:300]
training_ys = indices_to_one_hot(ys[:300], 2)
test_xs = xs[300:]
test_ys = ys[300:]


# nn.train(training_xs, training_ys)

# print(xs)
# print(training_ys)

accuracy(nn, test_xs, test_ys)
results = []

total_epochs = 1000
result_every = 10

for i in range(total_epochs):
    nn.train(training_xs, training_ys)

    if (i + 1) % result_every == 0:
        r = accuracy(nn, test_xs, test_ys)
        results.append(r)

# plot(test_xs, results)


