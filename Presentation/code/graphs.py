from sklearn.datasets import load_iris
import matplotlib.pyplot as plt
import seaborn as sns; sns.set_style("whitegrid")

iris = load_iris()

data = iris['data'][:100,:2]
targets = iris['target'][:100]

x = data[:, 0]
y = data[:, 1]

ax = sns.scatterplot(x=x, y=y, hue=targets)
ax.grid(False)
ax.set_xlim(4, 7.1)
ax.set_ylim(1, 5)

plt.show()
