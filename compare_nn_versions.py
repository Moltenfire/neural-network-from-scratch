import numpy as np
from copy import deepcopy
from nn import Network as NN0
from nn_mat import Network as NN1
from nn_utils import format_column

layers = (2, 4, 3)

def get_networks():
    network_0 = NN0(layers)
    network_1 = NN1(layers)

    for i in range(len(layers) - 1):
        network_1.weights[i] = np.copy(network_0.weights[i])
        network_1.biases[i] = np.copy(network_0.biases[i])

    # print(network_0.weights)
    # print(network_1.weights)
    # print(network_0.biases)
    # print(network_1.biases)

    return network_0, network_1

def run_network_0(x, y):
    print(network_0.forward(x))

    b, w = network_0.backwards(x, y)

    for i in range(len(w)):
        print("Layer", i)
        print("Weights", i)
        print(w[i])
        print("Biases", i)
        print(b[i])

def run_network_1(x, y):
    print(network_1.forward(x))

    b, w = network_1.backwards(x, y)

    for i in range(len(w)):
        print("Layer", i)
        print("Weights", i)
        print(w[i])
        print("Biases", i)
        print(b[i])

network_0, network_1 = get_networks()

x = np.array([0.2, 0.6])
y = np.array([1, 0, 0])

run_network_0(x, y)
run_network_1(x, y)
