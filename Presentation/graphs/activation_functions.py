import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
import seaborn
# seaborn.set(style='ticks')
seaborn.set(style='whitegrid')

# interface tracking profiles
N = 500
X = np.linspace(-3, 3, N)

Z = np.linspace(-1.1, 1.1, N)

plt.plot(X, np.tanh(X), label='tanh')
plt.plot(X, 1 / (1 + np.exp(-X)), label='sigmoid')
plt.plot(Z, Z, label='linear')

plt.ylim(-1.1, 1.1)
plt.xlim(-3, 3)

plt.axhline(linewidth=1, color='black', linestyle='-')
plt.axvline(linewidth=1, color='black', linestyle='-')

plt.axhline(-1, linewidth=1, color='black', linestyle='--')
plt.axhline(1, linewidth=1, color='black', linestyle='--')

seaborn.despine(left=True, bottom=True)

ax = plt.gca()
ax.xaxis.set_major_locator(MaxNLocator(integer=True))
ax.yaxis.set_major_locator(MaxNLocator(integer=True))

plt.xlabel("Input")
plt.ylabel("Output")

ax.legend(loc="upper left", bbox_to_anchor=(0, 0.9))

plt.show()
