import numpy as np

def format_column(l):
    return np.array([l]).transpose()
