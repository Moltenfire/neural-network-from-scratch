import numpy as np
from itertools import tee 
np.random.seed(0)

def get_random(size, min_val=0, max_val=1):
    return np.random.random_sample(size)
    # return np.random.random_sample(size) * (max_val - min_val) + min_val
    # return np.random.randn(*size)

def sigmoid(x):
  return 1.0 / (1.0 + np.exp(-x))

def sigmoid_prime(x):
    return sigmoid(x) * (1 - sigmoid(x))

def pairwise(iterable):
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)

class Network():

    def __init__(self, sizes):
        self.sizes = sizes
        self.num_layers = len(sizes)
        self.weights = []
        self.biases = []
        self.epoch = 0
        for s1, s2 in pairwise(self.sizes):
            w = get_random((s2, s1), -1, 1)
            b = get_random((s2, 1), -1, 1)
            self.weights.append(w)
            self.biases.append(b)

    def p(self):
        for w in self.weights:
            print(w)

    def forward(self, x):
        ax = [x]
        for layer in range(1, len(self.sizes)):
            a = []
            for i in range(self.sizes[layer]):
                res = 0
                for j in range(self.sizes[layer - 1]):
                    weight = self.weights[layer - 1][i, j]
                    value = ax[layer - 1][j]
                    res += weight * value
                bias = self.biases[layer - 1][i]
                res += bias
                res = sigmoid(res)
                a.append(res)
            ax.append(np.array(a))
        return ax[-1]

    def train(self, xs, ys):
        self.epoch += 1
        learning_rate = 0.1
        nabla_b = [np.zeros(b.shape) for b in self.biases]
        nabla_w = [np.zeros(w.shape) for w in self.weights]

        for x, y in zip(xs, ys):
            nabla_b_temp, nabla_w_temp = self.backwards(x, y)
            for layer in range(self.num_layers - 1):
                nabla_b[layer] += nabla_b_temp[layer]
                nabla_w[layer] += nabla_w_temp[layer]

        for layer in range(self.num_layers - 1):
            nabla_b[layer] /= len(xs)
            nabla_w[layer] /= len(xs)
            nabla_b[layer] *= learning_rate
            nabla_w[layer] *= learning_rate

        for layer in range(self.num_layers - 1):
            self.weights[layer] += nabla_w[layer]
            self.biases[layer] += nabla_b[layer]

    def backwards(self, x, y):
        nabla_b = [np.zeros(b.shape) for b in self.biases]
        nabla_w = [np.zeros(w.shape) for w in self.weights]
        # forward pass
        ax = [x]
        zs = []
        for layer in range(1, len(self.sizes)):
            a = []
            z = []
            for i in range(self.sizes[layer]):
                res = 0
                for j in range(self.sizes[layer - 1]):
                    weight = self.weights[layer - 1][i, j]
                    value = ax[layer - 1][j]
                    res += weight * value
                bias = self.biases[layer - 1][i]
                res += bias
                z.append(res)
                res = sigmoid(res)
                a.append(res)

            zs.append(z)
            ax.append(a)

        # last layer
        deltas = []
        for n in range(self.sizes[-1]):
            error = y[n] - ax[-1][n]
            delta = error * sigmoid_prime(zs[-1][n])
            deltas.append(delta)

            nabla_b[-1][n] = delta

            for i in range(self.sizes[-2]):
                nabla_w[-1][n, i] = delta * ax[-2][i]

        # layers 2 to n-1
        for layer in range(2, self.num_layers):
            prev_deltas = deltas
            deltas = []
            for n in range(self.sizes[-layer]):
                z = zs[-layer][n]
                sp = sigmoid_prime(z)
                d = 0
                for i in range(self.sizes[-layer+1]):
                    d += self.weights[-layer+1][i, n] * prev_deltas[i]
                delta = d * sp
                nabla_b[-layer][n] = delta
                for i in range(self.sizes[-layer-1]):
                    v = delta * ax[-layer-1][i]
                    nabla_w[-layer][n, i] = v
                deltas.append(delta)

        return nabla_b, nabla_w
