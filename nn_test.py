import pytest
import numpy as np
from copy import deepcopy
from nn import Network as NN1
from nn_mat import Network as NN2

LAYERS = (2, 4, 1)
MORE_LAYERS = (2, 4, 2, 1)

def get_nn(layers):
    nn1 = NN1(layers)
    nn2 = NN2(layers)

    nn2.weights = deepcopy(nn1.weights)
    nn2.biases = deepcopy(nn1.biases)

    return nn1, nn2

def test_nn_weights():
    nn1, nn2 = get_nn(LAYERS)

    for w1, w2 in zip(nn1.weights, nn2.weights):
        assert np.all(w1 == w2)

@pytest.mark.parametrize("x,layers", [
    (np.array([0.2, 0.6]), LAYERS), 
    (np.array([0.4, 0.6]), LAYERS),
    (np.array([0.8, 0.5]), LAYERS),
    (np.array([0.8, 0.3]), LAYERS),
    (np.array([0.8, 0.3]), MORE_LAYERS)
])
def test_nn_forward(x, layers):
    nn1, nn2 = get_nn(layers)
    assert nn1.forward(x) == nn2.forward(x)

@pytest.mark.parametrize("x,y,layers", [
    (np.array([0.2, 0.6]), np.array([0]), LAYERS),
    (np.array([0.2, 0.6]), np.array([1]), LAYERS),
    (np.array([0.8, 0.5]), np.array([0]), LAYERS),
    (np.array([0.8, 0.5]), np.array([1]), LAYERS),
    (np.array([0.8, 0.5]), np.array([1]), MORE_LAYERS),
])
def test_nn_backwards(x, y, layers):
    nn1, nn2 = get_nn(layers)

    nn1_b, nn1_w = nn1.backwards(x, y)
    nn2_b, nn2_w = nn2.backwards(x, y)

    for b1, b2 in zip(nn1_b, nn2_b):
        assert np.all(b1 == b2)

    for w1, w2 in zip(nn1_w, nn2_w):
        assert np.all(w1 == w2)

@pytest.mark.parametrize("x,y,layers", [
    (np.array([[0.2, 0.6], [0.3, 0.5]]), np.array([[0], [0]]), LAYERS)
])
def test_nn_train(x, y, layers):
    nn1, nn2 = get_nn(layers)

    assert np.array_equal(nn1.forward(x), nn2.forward(x))

    for _ in range(100):
        nn1.train(x, y)
        nn2.train(x, y)

    assert np.array_equal(nn1.forward(x), nn2.forward(x))
