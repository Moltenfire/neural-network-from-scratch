from calculations import backprop, epoch
from sklearn.datasets import load_iris
from pprint import pprint
from dataset import mse

import numpy as np

iris = load_iris()
data = iris['data'][:100,:2]
targets = np.array([[1,0] if t == 0 else [0,1] for t in iris['target'][:100]])

i = data[1]
target_o = targets[1]

w_0 = np.array([[-0.5, 0.2],
       [0.4, -0.1],
       [0.1, 0]])
w_1 = np.array([[0.73, 0.97, 0.85],
       [-0.23, -0.72, 0.17]])

b_0 = np.array([-0.2, 0, 0.1])
b_1 = np.array([0.73, -0.37])

new_w_0, new_w_1, new_b_0, new_b_1 = epoch(data, targets, w_0, w_1, b_0, b_1)

print(mse(data, targets, w_0, w_1, b_0, b_1))
print(mse(data, targets, new_w_0, new_w_1, new_b_0, new_b_1))


