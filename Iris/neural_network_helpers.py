import numpy as np
import matplotlib.pyplot as plt
from math import ceil

def indices_to_one_hot(data, nb_classes):
    targets = np.array(data).reshape(-1)
    return np.eye(nb_classes)[targets]

def accuracy(neural_network, test_xs, test_ys):
    result = np.array([np.argmax(neural_network.forward(x)) for x, y in zip(test_xs, test_ys)])
    count = np.sum(test_ys == result)
    total = len(test_xs)
    return count / total, result*2 + test_ys

def mse(neural_network, test_xs, test_ys):
    results = np.array([neural_network.forward(x) for x in test_xs])
    return np.average(np.square(results - test_ys))

def plot(test_xs, results):

    rows = ceil(len(results)/5)
    fig, ax = plt.subplots(nrows=rows, ncols=5, sharex=True, sharey=True)

    colours = {
        0 : '#ff0000',
        1 : '#770000',
        2 : '#007700',
        3 : '#00ff00',
        4 : '#000077',
        5 : '#0000ff'
    }

    for i, result in enumerate(results):
        # print(result)
        a, b = int(i/5), i % 5
        axis = ax[a,b] if rows > 1 else ax[b]
        l = axis.scatter(test_xs[:, 0], test_xs[:, 1], c=[colours[r] for r in result])

    plt.show()