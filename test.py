from nn_mat import Network as N1
from network_git import NetworkG as N2
from nn_utils import format_column
import numpy as np

# layers = (2, 3, 4, 3)
# layers = (2, 3)
layers = (2, 4, 3)

def get_networks():
    # print("N1 - Create")
    n1 = N1(layers)
    # print("N2 - Create")
    n2 = N2(layers)

    for i in range(len(n1.weights)):
        b = n1.biases[i]
        n2.biases[i] = np.copy(b)

    for i in range(len(n1.weights)):
        w = n1.weights[i]
        n2.weights[i] = np.copy(w)

    # print("Weights")
    # for i in range(len(n1.weights)):
    #     print(i, n1.weights[i].shape, n2.weights[i].shape)

    # print("Biases")
    # for i in range(len(n1.biases)):
    #     print(i, n1.biases[i].shape, n2.biases[i].shape)

    return n1, n2

def network1(x, y):
    # print(n1.forward(x))

    b, w = n1.backwards(x, y)
    for i in range(len(w)):
        print("Layer", i)
        print("Weights", i)
        print(w[i])
        print("Biases", i)
        print(b[i])

def network2(x, y):
    # print(n2.feedforward(x))

    b, w = n2.backprop(x, y)
    for i in range(len(w)):
        print("Layer", i)
        print("Weights", i)
        print(w[i])
        print("Biases", i)
        print(b[i])

n1, n2 = get_networks()

x = format_column([0.2, 0.4])
y = format_column([1, 0, 0])

print("N1")
network1(x, y)

print("N2")
network2(x, y)
