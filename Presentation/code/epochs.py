from calculations import epoch
from sklearn.datasets import load_iris
from pprint import pprint
from dataset import mse, correct
import numpy as np

def epochs():

    iris = load_iris()
    data = iris['data'][:100,:2]
    targets = np.array([[1,0] if t == 0 else [0,1] for t in iris['target'][:100]])

    w_0 = np.array([[-0.5, 0.2],
        [0.4, -0.1],
        [0.1, 0]])
    w_1 = np.array([[0.73, 0.97, 0.85],
        [-0.23, -0.72, 0.17]])
    b_0 = np.array([-0.2, 0, 0.1])
    b_1 = np.array([0.73, -0.37])

    yield 0, mse(data, targets, w_0, w_1, b_0, b_1), correct(data, targets, w_0, w_1, b_0, b_1), w_0, w_1, b_0, b_1


    for i in range(100):

        new_w_0, new_w_1, new_b_0, new_b_1 = epoch(data, targets, w_0, w_1, b_0, b_1)

        i, mean_square_error, c = (i+1, mse(data, targets, new_w_0, new_w_1, new_b_0, new_b_1), correct(data, targets, new_w_0, new_w_1, new_b_0, new_b_1))

        w_0, w_1, b_0, b_1 = new_w_0, new_w_1, new_b_0, new_b_1

        yield i, mean_square_error, c, w_0, w_1, b_0, b_1

def main():

    for i, mse, acc, _, _, _, _ in epochs():
        if i in list(range(101)[0::10]) or i == 1:
            print(i, mse, acc)

if __name__ == "__main__":
    main()
